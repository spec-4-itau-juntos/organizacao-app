package br.com.itau.organizacao.exception;

import br.com.itau.util.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class OrganizacaoNotFoundException extends NotFoundException {
    public OrganizacaoNotFoundException() {
        super("Organizacao não encontrada");
    }
}
