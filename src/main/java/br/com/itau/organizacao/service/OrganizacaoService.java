package br.com.itau.organizacao.service;

import br.com.itau.organizacao.DTO.OrganizacaoDTO;
import br.com.itau.organizacao.exception.OrganizacaoNotFoundException;
import br.com.itau.organizacao.model.Organizacao;
import br.com.itau.organizacao.repository.OrganizacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrganizacaoService {

    @Autowired
    OrganizacaoRepository organizacaoRepository;

    public List<Organizacao> getOrganizacoes() {
        return (List)organizacaoRepository.findAll();
    }


    public Organizacao getOrganizacaoPorId(Long id) {
        Optional<Organizacao> organizacaoDb = organizacaoRepository.findById(id);

        if(organizacaoDb.isPresent()){
            Organizacao response = organizacaoDb.get();
            return response;
        } else {
            throw new OrganizacaoNotFoundException();
        }
    }

    public Organizacao cadastraOrganizacao(OrganizacaoDTO organizacaoDTO) {

        Organizacao organizacaoDb = new Organizacao();

        organizacaoDb.setCnpj(organizacaoDTO.getCnpj());
        organizacaoDb.setNome(organizacaoDTO.getNome());

        return organizacaoRepository.save(organizacaoDb);
    }

    public Organizacao atualizaOrganizacao(Long id, OrganizacaoDTO organizacaoDTO) {

        Organizacao organizacaoDb = new Organizacao();

        if(organizacaoRepository.existsById(id)){
            organizacaoDb.setId(id);
            organizacaoDb.setNome(organizacaoDTO.getNome());
            organizacaoDb.setCnpj(organizacaoDTO.getCnpj());

            return organizacaoRepository.save(organizacaoDb);
        } else {
            throw new OrganizacaoNotFoundException();
        }
    }

    public void deletarOrganizacao(Long id) {
        if(organizacaoRepository.existsById(id)){
            organizacaoRepository.deleteById(id);
        } else {
            throw new OrganizacaoNotFoundException();
        }
    }
}
