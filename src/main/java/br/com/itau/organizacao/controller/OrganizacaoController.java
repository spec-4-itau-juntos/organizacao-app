package br.com.itau.organizacao.controller;

import br.com.itau.organizacao.DTO.OrganizacaoDTO;
import br.com.itau.organizacao.model.Organizacao;
import br.com.itau.organizacao.service.OrganizacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/organizacao")
@CrossOrigin
public class OrganizacaoController {

    @Autowired
    OrganizacaoService organizacaoService;

    @GetMapping
    public List<Organizacao> retornaOrganizacoes() {
        return organizacaoService.getOrganizacoes();
    }

    @GetMapping("/{id}")
    public Organizacao getOrganizacaoPorId(@PathVariable(name = "id") Long id) {
        Organizacao organizacao = organizacaoService.getOrganizacaoPorId(id);
        return organizacao;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Organizacao cadastraOrganizacao(@RequestBody OrganizacaoDTO organizacaoDTO) {
        return organizacaoService.cadastraOrganizacao(organizacaoDTO);
    }

    @PatchMapping("/{id}")
    public Organizacao atualizaOrganizacao(@PathVariable("id") Long id, @RequestBody OrganizacaoDTO organizacaoDTO) {
        return organizacaoService.atualizaOrganizacao(id, organizacaoDTO);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarOrganizacao(@PathVariable("id") Long id) {
        organizacaoService.deletarOrganizacao(id);
    }
}
