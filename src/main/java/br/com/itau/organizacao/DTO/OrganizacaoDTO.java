package br.com.itau.organizacao.DTO;

import javassist.NotFoundException;
import org.hibernate.validator.constraints.br.CNPJ;

import javax.validation.constraints.NotBlank;

public class OrganizacaoDTO {

    @NotBlank(message = "O nome da organização não pode ser nulo ou vazio!")
    private String nome;

    @CNPJ(message = "CNPJ inválido")
    private String cnpj;

    public OrganizacaoDTO(String nome, String cnpj) {
        this.nome = nome;
        this.cnpj = cnpj;
    }

    public OrganizacaoDTO(){}

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
}
