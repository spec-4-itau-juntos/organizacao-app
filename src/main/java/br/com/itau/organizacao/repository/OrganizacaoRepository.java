package br.com.itau.organizacao.repository;

import br.com.itau.organizacao.model.Organizacao;
import org.springframework.data.repository.CrudRepository;

public interface OrganizacaoRepository extends CrudRepository<Organizacao, Long> {
}
