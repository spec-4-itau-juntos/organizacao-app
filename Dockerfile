FROM openjdk:8-jre-alpine
COPY target/organizacao-app-*.jar organizacao-app.jar
COPY /appagent/ ./

CMD [ "java", "-javaagent:/javaagent.jar", "-jar", "organizacao-app.jar"]